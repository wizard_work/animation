//
//  ViewController.swift
//  Animation
//
//  Created by Yurii Goroshenko on 12.06.2020.
//  Copyright © 2020 Yurii Goroshenko. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  @IBOutlet weak var animationView: AnimationView!
  
  // MARK: - Lifecycle
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    animationView.startLoading()
  }
}

