//
//  AnimationItemView.swift
//  Animation
//
//  Created by Yurii Goroshenko on 12.06.2020.
//  Copyright © 2020 Yurii Goroshenko. All rights reserved.
//

import UIKit

class AnimationItemView: UIView {
  
  // MARK: - Properties
  private var itemCornerRadius: CGFloat = 4.0
  var index: Int = 0
  var moveSide: AnimationSide = .left
  var jumpSide: AnimationJumpSide = .bottom
  
  // MARK: - Lifecycle
  init(frame: CGRect, color: UIColor?, at index: Int) {
    super.init(frame: frame)
    self.index = index
    self.bounds.size = frame.size
    self.backgroundColor = color
    self.layer.cornerRadius = itemCornerRadius
    self.clipsToBounds = true
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
