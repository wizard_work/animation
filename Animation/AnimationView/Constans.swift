//
//  Constans.swift
//  Animation
//
//  Created by Yurii Goroshenko on 12.06.2020.
//  Copyright © 2020 Yurii Goroshenko. All rights reserved.
//

import UIKit

struct Colors {
  static let blue1: UIColor = UIColor(red: 26.0 / 255.0, green: 148.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0) // #1A94FF
  static let blue2: UIColor = UIColor(red: 28.0 / 255.0, green: 163.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0) // #1CA3FF
  static let blue3: UIColor = UIColor(red: 32.0 / 255.0, green: 185.0 / 255.0, blue: 253.0 / 255.0, alpha: 1.0) // #20B9FD
  static let blue4: UIColor = UIColor(red: 36.0 / 255.0, green: 194.0 / 255.0, blue: 240.0 / 255.0, alpha: 1.0) // #25CBFB
}

enum AnimationSide {
  case left
  case right
}

enum AnimationJumpSide {
  case bottom
  case top
}
