//
//  AnimationView.swift
//  Animation
//
//  Created by Yurii Goroshenko on 12.06.2020.
//  Copyright © 2020 Yurii Goroshenko. All rights reserved.
//

import UIKit



final class AnimationView: UIView {
  private var colors: [UIColor] = [Colors.blue1, Colors.blue2, Colors.blue3, Colors.blue4]
  private var moveItemView: AnimationItemView!
  private var jumpItemViews: [AnimationItemView] = []
  
  private var itemSize: CGSize = CGSize(width: 20.0, height: 20.0)
  private var itemMaxJumpHeight: CGFloat = 30.0
  private var itemOffSet: CGFloat  { return itemMaxJumpHeight/2 }
  
  private let moveAnimationDuration: Double = 1.0
  private let compressAnimationDuration: Double = 0.1
  private let compressPercent: Double = 0.25 // 20%
  
  
  // MARK: - Lifecycle
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setupAnimationView()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  // MARK: - Private functions
  // MARK: - Configure views
  private func createItemView(at index: Int, color: UIColor?) -> AnimationItemView {
    let frame = getItemPosition(at: index)
    let view = AnimationItemView(frame: frame, color: color, at: index)
    view.moveSide = .left
    view.jumpSide = .bottom
    self.addSubview(view)
    return view
  }
  
  private func setupAnimationView() {
    jumpItemViews.removeAll()
    
    var index = 0
    while index < colors.count {
      jumpItemViews.append(createItemView(at: index, color: colors[index]))
      index += 1
    }
    moveItemView = createItemView(at: colors.count, color: colors.last)
    moveItemView.moveSide = .right
    moveItemView.jumpSide = .bottom
  }
  
  private func getItemPosition(at index: Int) -> CGRect {
    var frame = CGRect.zero
    frame.size = itemSize
    if index > 0 {
      frame.origin.x = (frame.size.width + itemOffSet) * CGFloat(index)
    }
    frame.origin.y = itemMaxJumpHeight
    
    return frame
  }
  
  // MARK: - Timers
  private func createMoveTimer() {
    let timer = Timer(timeInterval: 0.0 , target: self, selector: #selector(startMoveAnimation), userInfo: nil, repeats: false)
    RunLoop.current.add(timer, forMode: .common)
  }
  
  // MARK: - Animation
  @objc private func startMoveAnimation() {
    //print("move from index = \(moveItemView.index) at side = \(moveItemView.moveSide)")
    let moveToSide: AnimationSide = moveItemView.moveSide == .left ? .right : .left
    let duration = moveAnimationDuration
    startJumpTimers(duration)
    
    let frame = getFrameMoveItemToSide(moveToSide)
    let backgroundColor = moveItemView.moveSide == .right ? colors.last : colors.first
    
    UIView.animate(withDuration: duration, animations: { [weak self] in
      self?.moveItemView.frame = frame
      self?.moveItemView.backgroundColor = backgroundColor
    }) { [weak self] (result) in
      guard let welf = self else { return }
      welf.moveItemView.moveSide = moveToSide
      welf.moveItemView.index = moveToSide == AnimationSide.left ? 0 : welf.colors.count
      //print("Finish Move")
      
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
        self?.createMoveTimer()
      }
    }
    
  }
  
  @objc private func startJumpAnimations(at index: Int, duration: Double) {
    let view = jumpItemViews[index]
    let startIndex = moveItemView.moveSide == .left ? index + 1 : colors.count - index
    let jumpDuration = duration/Double(colors.count)
    let timeInterval = (jumpDuration/2 + 0.01)*Double(startIndex-1)
    //print("Jump \(timeInterval) at index:\(index) at start: \(startIndex)")
    
    DispatchQueue.main.asyncAfter(deadline: .now() + timeInterval) { [weak self] in
      guard let welf = self else { return }
      welf.startJumpAnimation(view, duration: jumpDuration)
    }
  }
  
  private func startJumpTimers(_ duration: Double) {
    var index = 0
    while index < colors.count {
      startJumpAnimations(at: index, duration: duration)
      index += 1
    }
  }
  
  private func getFrameMoveItemToSide(_ side: AnimationSide) -> CGRect {
    var frame = moveItemView.frame
    switch side {
    case .left: frame.origin.x = 0.0
    case .right: frame.origin.x = (frame.size.width + itemOffSet) * CGFloat(colors.count)
    }
    
    return frame
  }
  
  private func getJumpItemToSide(_ view: AnimationItemView, side: AnimationSide, jumpSide: AnimationJumpSide) -> CGRect {
    //  print("move from index = \(view.index) at side = \(view.moveSide) at jump = \(view.jumpSide)")
    var frame = view.frame
    
    switch jumpSide {
    case .bottom: frame.origin.y = itemMaxJumpHeight
    case .top: frame.origin.y = 0.0
    }
    
    let moveOffSet = (frame.size.width + itemOffSet) / 2
    switch side {
    case .left: frame.origin.x -= moveOffSet
    case .right: frame.origin.x += moveOffSet
    }
    
    return frame
  }
  
  private func startJumpAnimation(_ view: AnimationItemView?, duration: Double) {
    guard let view = view else { return }
    let moveSide: AnimationSide = view.moveSide == .right ? .left : .right
    let rotatedValue: CGFloat = moveItemView.moveSide == .right ? .pi/2 : -(.pi/2)
    let topFrame = getJumpItemToSide(view, side: moveSide, jumpSide: .top)
    
    UIView.animate(withDuration: duration, animations: {
      view.transform = view.transform.rotated(by: rotatedValue)
      view.frame = topFrame
    }) { [weak self] (result) in
      guard let welf = self else { return }
      view.jumpSide = .top
      let bottomFrame = welf.getJumpItemToSide(view, side: moveSide, jumpSide: .bottom)
      
      UIView.animate(withDuration: duration, animations: {
        view.transform = view.transform.rotated(by: rotatedValue)
        view.frame = bottomFrame
      }) { [weak self] (result) in
        view.jumpSide = .bottom
        view.moveSide = moveSide
        //print("Finish Jump")
        self?.compressItem(view)
      }
    }
  }
  
  private func compressItem(_ view: AnimationItemView) {
    let duration = compressAnimationDuration
    let frame = view.frame
    var compressFrame = frame
    compressFrame.size.height = compressFrame.size.height*CGFloat(1-compressPercent)
    compressFrame.origin.y = compressFrame.origin.y + (frame.height - compressFrame.height)
    
    UIView.animate(withDuration: duration, animations: {
      view.frame = compressFrame
    }) { (result) in
      UIView.animate(withDuration: duration, animations: {
        view.frame = frame
      })
      //print("Finish compress")
    }
  }
  
  // MARK: - Public functions
  func startLoading() {
    createMoveTimer()
  }
}
